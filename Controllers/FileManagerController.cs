﻿using GleamTech.FileSystems;
using GleamTech.FileUltimate.AspNet.UI;
using GleamTech.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using prueba.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace prueba.Controllers
{
    public class FileManagerController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Overview()
        {
            var fileManager = new FileManager
            {
                Width = 800,
                Height = 600,
                Resizable = true
            };
            //var rootFolder = @"Path=\\server\share; Authenticated User=Windows";
            var rootFolder = new FileManagerRootFolder();
            rootFolder.Name = "compartido";
            rootFolder.Location = @"Path='\\utegrupos\tic\odi\sopaplica1\Documentos compartidos\GENERAL\'; User Name=f245277@corp.ute.com.uy; Password='3Zavijah6'";
            //     {
            //         Name = "A Root Folder",
            //         Location = @"\\utegrupos\tic\odi\sopaplica1\Documentos compartidos; Authenticated User=Windows"User Name=f245277@corp.ute.com.uy; Password='3Zavijah6'
            //         //AuthenticatedUser = AuthenticatedUser.Windows
            //         //UserName = "f245277",
            //         //Password = "3Zavijah6"
            //      };

            rootFolder.AccessControls.Add(new FileManagerAccessControl
            {
                Path = @"\",
                AllowedPermissions = FileManagerPermissions.Full
            });

            fileManager.RootFolders.Add(rootFolder);

            fileManager.RootFolders.Add(CreateRootFolder2());



            return View(fileManager);
        }


        private FileManagerRootFolder CreateRootFolder2()
        {


            //var rootFolder = @"Path=\\server\share; Authenticated User=Windows";
            var rootFolder = new FileManagerRootFolder();
            rootFolder.Name = "mipc";
            rootFolder.Location = @"Path='\\pc161214\compartido'";

            rootFolder.AccessControls.Add(new FileManagerAccessControl
            {
                Path = @"\",
                AllowedPermissions = FileManagerPermissions.Full
            });

           

            return rootFolder;

            
        }






    }
}
